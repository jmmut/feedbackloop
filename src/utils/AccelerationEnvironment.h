/**
 * @file Environment.h
 * @author jmmut
 * @date 2017-04-11.
 */

#ifndef FEEDBACKLOOP_ACCELERATION_ENVIRONMENT_H
#define FEEDBACKLOOP_ACCELERATION_ENVIRONMENT_H


#include "blackbox/Blackbox.h"
#include "blackbox/TimeBlackbox.h"

class AccelerationEnvironment : public TimeBlackbox {
public:
    AccelerationEnvironment(double status);

    virtual ~AccelerationEnvironment() override {}

    using TimeBlackbox::process;
    virtual double process(double acceleration, double time) override;
    virtual double getStatus();

protected:
    double position;
    double speed;
};


#endif //FEEDBACKLOOP_SPEED_ENVIRONMENT_H
