/**
 * @file Environment.h
 * @author jmmut
 * @date 2017-04-11.
 */

#ifndef FEEDBACKLOOP_SPEED_ENVIRONMENT_H
#define FEEDBACKLOOP_SPEED_ENVIRONMENT_H


#include "blackbox/Blackbox.h"
#include "blackbox/TimeBlackbox.h"

class SpeedEnvironment : public TimeBlackbox {
public:
    SpeedEnvironment(double status);

    virtual ~SpeedEnvironment() override {}

    using TimeBlackbox::process;
    virtual double process(double speed, double time) override;
    virtual double getStatus();

protected:
    double position;
};


#endif //FEEDBACKLOOP_SPEED_ENVIRONMENT_H
