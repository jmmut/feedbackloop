/**
 * @file Physics.h
 * @author jmmut
 * @date 2017-04-28.
 */

#ifndef FEEDBACKLOOP_PHYSICS_H
#define FEEDBACKLOOP_PHYSICS_H

namespace Physics {

    double computePosition(double acceleration, double speed, double position, double t);

    double computeSpeed(double acceleration, double speed, double t);

};
#endif //FEEDBACKLOOP_PHYSICS_H
