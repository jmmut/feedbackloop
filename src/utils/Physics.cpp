/**
 * @file Physics.cpp
 * @author jmmut
 * @date 2017-04-28.
 */

#include "Physics.h"

double Physics::computeSpeed(double acceleration, double speed, double t) {
    return acceleration * t + speed;
}

double Physics::computePosition(double acceleration, double speed, double position, double t) {
    return 0.5 * acceleration * t * t + speed * t + position;
}
