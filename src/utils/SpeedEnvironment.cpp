/**
 * @file Environment.cpp
 * @author jmmut
 * @date 2017-04-11.
 */

#include "SpeedEnvironment.h"
#include "utils/Physics.h"

SpeedEnvironment::SpeedEnvironment(double status) : position(status) {}

double SpeedEnvironment::process(double speed, double time) {
    position = Physics::computePosition(0, speed, position, time);
    return position;
}

double SpeedEnvironment::getStatus() {
    return position;
}

