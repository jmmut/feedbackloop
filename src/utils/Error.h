/**
 * @file Error.h
 * @author jmmut
 * @date 2017-04-29.
 */

#ifndef FEEDBACKLOOP_ERROR_H
#define FEEDBACKLOOP_ERROR_H


#include <blackbox/Blackbox.h>

class Error : public Blackbox {
public:
    Error(double goal);

    virtual double process(double input) override;

private:
    double goal;
};


#endif //FEEDBACKLOOP_ERROR_H
