/**
 * @file Deriver.cpp
 * @author jmmut
 * @date 2017-04-26.
 */

#include "Deriver.h"

double Deriver::process(double input, double time) {
    double delta = input - lastInput;
    lastInput = input;
    return delta / time;
}
