/**
 * @file Deriver.h
 * @author jmmut
 * @date 2017-04-26.
 */

#ifndef FEEDBACKLOOP_DERIVER_H
#define FEEDBACKLOOP_DERIVER_H


#include "blackbox/TimeBlackbox.h"

class Deriver : public TimeBlackbox {
public:
    virtual ~Deriver() override {}

    using TimeBlackbox::process;
    virtual double process(double input, double time) override;
private:
    double lastInput = 0;
};


#endif //FEEDBACKLOOP_DERIVER_H
