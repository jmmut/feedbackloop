/**
 * @file Environment.cpp
 * @author jmmut
 * @date 2017-04-11.
 */

#include "AccelerationEnvironment.h"
#include "utils/Physics.h"

AccelerationEnvironment::AccelerationEnvironment(double status) : position(status), speed(0) {}

double AccelerationEnvironment::process(double acceleration, double time) {
    position = Physics::computePosition(acceleration, speed, position, time);
    speed = Physics::computeSpeed(acceleration, speed, time);
    return position;
}

double AccelerationEnvironment::getStatus() {
    return position;
}

