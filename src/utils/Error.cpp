/**
 * @file Error.cpp
 * @author jmmut
 * @date 2017-04-29.
 */

#include "Error.h"

Error::Error(double goal) : goal(goal) {}

double Error::process(double input) {
    return goal - input;
}
