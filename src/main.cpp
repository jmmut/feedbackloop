#include <utils/SignalHandler.h>
#include <utils/SpeedEnvironment.h>
#include <utils/Physics.h>
#include <utils/Walkers.h>
#include "events/Dispatcher.h"
#include "events/EventCallThis.h"
#include "manager_templates/FlyerManagerBase.h"
#include "controllers/FeedbackControl.h"
#include "utils/AccelerationEnvironment.h"
#include "controllers/MemoryFeedbackControl.h"
#include "utils/Deriver.h"
#include "controllers/SofterFeedbackControl.h"

const double alpha = 1;

class TestManager: public FlyerManagerBase{
public:
    TestManager( Dispatcher &d, std::shared_ptr< WindowGL> w)
            : FlyerManagerBase( d, w), feedbackControl(-2, alpha), speedFeedbackControl(-2, alpha)
            , environment(2), speedEnvironment(2)
            , isControlActive(active), paused(false) {
        glPointSize(10);
        int deriversCount = 5;
        derivers.resize(deriversCount);
        plots.resize(2 * deriversCount);
    };
protected:
    void onStep(Uint32 ms) override {
        MediumManagerBase::onStep(ms);
        if (not paused) {

            double seconds = ms / 1000.0;
//            const double seconds = 1;
            double gravity = -2;
//            const double gravity = -1/1024.0/4;

            {
                double acceleration = gravity;
                double output = 0;
                if (isControlActive) {
                    double status = environment.getStatus();
                    output = feedbackControl.process(status, seconds);
                    acceleration += output;
                }
                environment.process(acceleration, seconds);

                std::vector<double> derivatives;
                double previousValue = environment.getStatus();
                for (size_t i = 0; i < derivers.size(); ++i) {
                    derivatives.push_back(previousValue);
                    plots[i].push_back(previousValue);
                    previousValue = derivers[i].process(previousValue, seconds);
                }
                LOG_DEBUG("output %f, gravity %f, derivatives: %s"
                        , output, gravity, randomize::utils::container_to_string(derivatives).c_str());
            }
/*
            {
                static double speed = Physics::computeSpeed(gravity, 0, seconds);
                speed = Physics::computeSpeed(gravity, speed, seconds);
                double output = 0;
                if (isControlActive) {
                    double status = speedEnvironment.getStatus();
                    output = speedFeedbackControl.process(status, seconds);
                    speed += output;
                }
                speedEnvironment.process(speed, seconds);
                double actualSpeed = one.process(speedEnvironment.getStatus(), seconds);
                double actualAccel = two.process(actualSpeed, seconds);
                double actualJerk = three.process(actualAccel, seconds);
                LOG_DEBUG("accel %f, speed %f, position %f, output %f, gravity %f",
                        actualAccel, actualSpeed, speedEnvironment.getStatus(), output, gravity);
                plots[4 + 0].push_back(speedEnvironment.getStatus());
                plots[4 + 1].push_back(actualSpeed);
                plots[4 + 2].push_back(actualAccel);
                plots[4 + 3].push_back(actualJerk);
            }*/
        }
    }

    void onDisplay( Uint32 ms) override {
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        glPushMatrix();

        this->FlyerManagerBase::onDisplay( ms);

        glBegin(GL_LINES);
        glColor3f(1, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(100, 0, 0);

        glColor3f(0, 1, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 100, 0);

        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, 100);
        glEnd();

        glBegin(GL_POINTS);
        glColor3f(0, isControlActive, not isControlActive);
        glVertex3f(3, environment.getStatus(), 3);
        glEnd();

        glBegin(GL_POINTS);
        glColor3f(0, isControlActive, not isControlActive);
        glVertex3f(6, speedEnvironment.getStatus(), 3);
        glEnd();

        GLfloat plotColors[][3] = {
                {0.1, 0.8, 0.5},
                {0.6, 0.4, 0.1},
                {0.3, 0.2, 0.6},
                {0.8, 0.2, 0.9},
                {0.5, 0.1, 0.8},
                {0.1, 0.6, 0.4},
                {0.6, 0.3, 0.2},
                {0.9, 0.8, 0.2},
                {0.8, 0.5, 0.1},
                {0.4, 0.1, 0.6},
                {0.2, 0.6, 0.3},
                {0.2, 0.9, 0.8}
        };
        glBegin(GL_POINTS);
        int i = 0;
        for (auto &&plot : plots) {
            glColor3fv(plotColors[i]);
            size_t j = 0;
            for (auto &&point : plot) {
                glVertex3f(j*0.1, point, -i*0.5 );
                ++j;
            }
            ++i;
        }
        glEnd();

        glPopMatrix();
        std::static_pointer_cast< WindowGL>( this->window.lock())->swapWindow();
    }

    void onKeyChange(SDL_KeyboardEvent &event) override {
        if (event.type == SDL_KEYDOWN and event.keysym.sym == SDLK_z) {
            isControlActive = not isControlActive;
        }
        if (event.type == SDL_KEYDOWN and event.keysym.sym == SDLK_SPACE) {
            paused = not paused;
        }
        if (event.type == SDL_KEYDOWN and event.keysym.sym == SDLK_r) {
            for (auto &plot : plots) {
                plot.clear();
            }
        }
    }

private:
    SofterFeedbackControl feedbackControl;
    SofterFeedbackControl speedFeedbackControl;
    std::vector<Deriver> derivers;
    AccelerationEnvironment environment;
    SpeedEnvironment speedEnvironment;
    bool isControlActive, paused;
    std::vector<std::vector<double>> plots;
};

int main(int argc, char **argv){
    SignalHandler::activate();
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_INFO_LEVEL);

    Dispatcher dispatcher;
    dispatcher.addEventHandler(
            [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();},
            [] (SDL_Event &event){ return event.type == SDL_QUIT;});

    std::shared_ptr< WindowGL> window = std::make_shared< WindowGL>();
    window->open();
    window->initGL();
    Uint32 windowID = window->getWindowID();
    dispatcher.addEventHandler(
            [&dispatcher,&window] (SDL_Event &event){ window->close(); dispatcher.endDispatcher();},
            [=] (SDL_Event &event){
                return (event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE)
                        || (event.type == SDL_KEYDOWN
                        && event.key.keysym.sym == SDLK_ESCAPE);
            });

    TestManager manager( dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();

    exit(0);
}

