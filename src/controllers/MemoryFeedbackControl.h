/**
 * @file MemoryFeedbackControl.h
 * @author jmmut
 * @date 2017-04-11.
 */

#ifndef FEEDBACKLOOP_MEMORYFEEDBACKCONTROL_H
#define FEEDBACKLOOP_MEMORYFEEDBACKCONTROL_H


#include "FeedbackControl.h"

class MemoryFeedbackControl : public FeedbackControl {
public:
    MemoryFeedbackControl(double goal);
    virtual ~MemoryFeedbackControl() override {}

    virtual double process(double input, double time) override;

private:
    double lastInput, lastOutput;
    bool firstIteration;
};


#endif //FEEDBACKLOOP_MEMORYFEEDBACKCONTROL_H
