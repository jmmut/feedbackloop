/**
 * @file MemoryFeedbackControl.cpp
 * @author jmmut
 * @date 2017-04-11.
 */

#include "MemoryFeedbackControl.h"

MemoryFeedbackControl::MemoryFeedbackControl(double goal)
        : FeedbackControl(goal), lastInput(0), lastOutput(0), firstIteration(true) {}


double MemoryFeedbackControl::process(double input, double time) {
    double delta = error.process(input);
    double targetImprovement = delta/2.0;

    if (firstIteration) {
        firstIteration = false;
        lastInput = input;
        lastOutput = targetImprovement / time;
        return lastOutput;
    } else {
        double change = input - lastInput;
//        double lastDelta = error.process(lastInput);
        double output = 0;

        if (change != 0) {
            output = targetImprovement * lastOutput / change / time;
        }
        lastOutput = output;
        lastInput = input;
        return output;
    }
}
