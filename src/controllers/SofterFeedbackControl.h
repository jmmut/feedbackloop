/**
 * @file HalfwayFeedbackControl.h
 * @author jmmut
 * @date 2017-04-27.
 */

#ifndef FEEDBACKLOOP_HALFWAYFEEDBACKCONTROL_H
#define FEEDBACKLOOP_SOFTERFEEDBACKCONTROL_H


#include "FeedbackControl.h"

class SofterFeedbackControl : public FeedbackControl {
public:
    SofterFeedbackControl(double goal, double coefficient);

    virtual double process(double input, double timeInterval) override;

private:
    double coefficient;
};


#endif //FEEDBACKLOOP_HALFWAYFEEDBACKCONTROL_H
