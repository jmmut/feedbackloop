/**
 * @file FeedbackControl.h
 * @author jmmut
 * @date 2017-04-11.
 */

#ifndef FEEDBACKLOOP_FEEDBACKCONTROL_H
#define FEEDBACKLOOP_FEEDBACKCONTROL_H


#include <vector>
#include <cstddef>
#include <utils/Error.h>
#include <utils/Deriver.h>
#include "blackbox/Blackbox.h"

class FeedbackControl : public TimeBlackbox {
public:
    FeedbackControl(double goal);

    virtual ~FeedbackControl() override;

    using TimeBlackbox::process;
    virtual double process(double input, double timeInterval) override;

protected:
    Error error;
    std::vector<Deriver> derivers;
    size_t iterations;
};


#endif //FEEDBACKLOOP_FEEDBACKCONTROL_H
