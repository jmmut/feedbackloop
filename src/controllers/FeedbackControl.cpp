/**
 * @file FeedbackControl.cpp
 * @author jmmut
 * @date 2017-04-11.
 */

#include <log/log.h>
#include <cmath>
#include "FeedbackControl.h"
#include "utils/Walkers.h"

FeedbackControl::FeedbackControl(double goal) : error(goal), derivers(3), iterations(0) {

}

FeedbackControl::~FeedbackControl() {

}

double FeedbackControl::process(double input, double timeInterval) {
    double difference = error.process(input);
    double output = 0;
    std::vector<double> differences;
    differences.push_back(difference);

    for (size_t i = 0; i < derivers.size(); ++i) {
        difference = derivers[i].process(difference, timeInterval);
        if (iterations > i) {
            differences.push_back(difference);
        }
    }
    if (iterations >= derivers.size()) {
        output = std::accumulate(differences.begin(), differences.end(), 0.0);
    }
    ++iterations;

    LOG_DEBUG("t=%f: error and derivatives: %s", timeInterval,
            randomize::utils::container_to_string(differences).c_str());

    return output;
}
