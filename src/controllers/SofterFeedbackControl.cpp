/**
 * @file HalfwayFeedbackControl.cpp
 * @author jmmut
 * @date 2017-04-27.
 */

#include <cmath>
#include "SofterFeedbackControl.h"

SofterFeedbackControl::SofterFeedbackControl(double goal, double coefficient)
        : FeedbackControl(goal), coefficient(coefficient) {}

double SofterFeedbackControl::process(double input, double timeInterval) {
    return FeedbackControl::process(input, timeInterval)
            * coefficient
//            * sqrt(sqrt(timeInterval * timeInterval))
            * timeInterval
            ;
}
