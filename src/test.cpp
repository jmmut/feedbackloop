/**
 * @file test.cpp
 * @author jmmut
 * @date 2017-04-26.
 */

#include <utils/SignalHandler.h>
#include <utils/test/TestSuite.h>
#include "utils/Deriver.h"
#include "controllers/MemoryFeedbackControl.h"
#include "utils/AccelerationEnvironment.h"

using namespace randomize::utils;

int main(int argc, char **argv) {
    test::TestSuite suite({
            []() {
                Deriver deriver;
                double output = deriver.process(1);
                ASSERT_EQUALS_OR_THROW(output, 1);
                output = deriver.process(3);
                ASSERT_EQUALS_OR_THROW(output, 2);
                output = deriver.process(-1);
                ASSERT_EQUALS_OR_THROW(output, -4);
            },
            []() {
                Deriver first;
                Deriver second;
                double firstOutput = first.process(0.5);
                double secondOutput = second.process(firstOutput);
                ASSERT_EQUALS_OR_THROW(firstOutput, 0.5);
                ASSERT_EQUALS_OR_THROW(secondOutput, 0.5);

                firstOutput = first.process(2);
                secondOutput = second.process(firstOutput);
                ASSERT_EQUALS_OR_THROW(firstOutput, 1.5);
                ASSERT_EQUALS_OR_THROW(secondOutput, 1);

                firstOutput = first.process(4.5);
                secondOutput = second.process(firstOutput);
                ASSERT_EQUALS_OR_THROW(firstOutput, 2.5);
                ASSERT_EQUALS_OR_THROW(secondOutput, 1);

                firstOutput = first.process(8);
                secondOutput = second.process(firstOutput);
                ASSERT_EQUALS_OR_THROW(firstOutput, 3.5);
                ASSERT_EQUALS_OR_THROW(secondOutput, 1);
            },
            [] () {
                FeedbackControl feedbackControl(0);
                Deriver first;
                Deriver second;

                double action = feedbackControl.process(0.5);
                double speed = first.process(action);
                double acceleration = second.process(speed);
                ASSERT_EQUALS_OR_THROW(action, -0.5);
                ASSERT_EQUALS_OR_THROW(speed, -0.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -0.5);

                action = feedbackControl.process(2);
                speed = first.process(action);
                acceleration = second.process(speed);
                ASSERT_EQUALS_OR_THROW(action, -2);
                ASSERT_EQUALS_OR_THROW(speed, -1.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -1);

                action = feedbackControl.process(4.5);
                speed = first.process(action);
                acceleration = second.process(speed);
                ASSERT_EQUALS_OR_THROW(action, -4.5);
                ASSERT_EQUALS_OR_THROW(speed, -2.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -1);

                action = feedbackControl.process(8);
                speed = first.process(action);
                acceleration = second.process(speed);
                ASSERT_EQUALS_OR_THROW(action, -8);
                ASSERT_EQUALS_OR_THROW(speed, -3.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -1);

                action = feedbackControl.process(12.5);
                speed = first.process(action);
                acceleration = second.process(speed);
                ASSERT_EQUALS_OR_THROW(action, -12.5);
                ASSERT_EQUALS_OR_THROW(speed, -4.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -1);
            },
            [] () {
                FeedbackControl feedbackControl(0);
                Deriver first;
                Deriver second;
                AccelerationEnvironment environment(0.5);
                double position = environment.getStatus();

                double action = feedbackControl.process(position);
                double speed = first.process(action);
                double acceleration = second.process(speed);
                position = environment.process(acceleration);
                ASSERT_EQUALS_OR_THROW(action, -0.5);
                ASSERT_EQUALS_OR_THROW(speed, -0.5);
                ASSERT_EQUALS_OR_THROW(acceleration, -0.5);
                ASSERT_EQUALS_OR_THROW(position, 0.25);

                action = feedbackControl.process(position);
                speed = first.process(action);
                acceleration = second.process(speed);
                position = environment.process(acceleration);
                ASSERT_EQUALS_OR_THROW(action, -0.25);
                ASSERT_EQUALS_OR_THROW(speed, 0.25);
                ASSERT_EQUALS_OR_THROW(acceleration, 0.75);
                ASSERT_EQUALS_OR_THROW(position, 0.625);

                action = feedbackControl.process(position);
                speed = first.process(action);
                acceleration = second.process(speed);
                position = environment.process(acceleration);
                ASSERT_EQUALS_OR_THROW(action, -0.625);
                ASSERT_EQUALS_OR_THROW(speed, -0.375);
                ASSERT_EQUALS_OR_THROW(acceleration, -0.625);
                ASSERT_EQUALS_OR_THROW(position, 0.3125);

                action = feedbackControl.process(position);
                speed = first.process(action);
                acceleration = second.process(speed);
                position = environment.process(acceleration);
                ASSERT_EQUALS_OR_THROW(action, -0.3125);
                ASSERT_EQUALS_OR_THROW(speed, 0.3125);
                ASSERT_EQUALS_OR_THROW(acceleration, 0.6875);
                ASSERT_EQUALS_OR_THROW(position, 0.65625);

                action = feedbackControl.process(position);
                speed = first.process(action);
                acceleration = second.process(speed);
                position = environment.process(acceleration);
                ASSERT_EQUALS_OR_THROW(action, -0.65625);
                ASSERT_EQUALS_OR_THROW(speed, -0.34375);
                ASSERT_EQUALS_OR_THROW(acceleration, -0.65625);
                ASSERT_EQUALS_OR_THROW(position, 0.328125);
            }
    }, "deriver");
    return 0;
}
