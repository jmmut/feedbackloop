/**
 * @file TimeBlackbox.h
 * @author jmmut
 * @date 2017-04-29.
 */

#ifndef FEEDBACKLOOP_TIMEBLACKBOX_H
#define FEEDBACKLOOP_TIMEBLACKBOX_H


#include "blackbox/Blackbox.h"

class TimeBlackbox : public Blackbox {
public:
    virtual ~TimeBlackbox() { }

    virtual double process(double input) override {
        return process(input, 1.0);
    }

    virtual double process(double input, double timeInterval) = 0;
};


#endif //FEEDBACKLOOP_TIMEBLACKBOX_H
