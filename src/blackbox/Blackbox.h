/**
 * @file Blackbox.h
 * @author jmmut
 * @date 2017-04-11.
 */

#ifndef FEEDBACKLOOP_BLACKBOX_H
#define FEEDBACKLOOP_BLACKBOX_H


class Blackbox {
public:
    virtual ~Blackbox() {}

    virtual double process(double input) = 0;
};


#endif //FEEDBACKLOOP_BLACKBOX_H
